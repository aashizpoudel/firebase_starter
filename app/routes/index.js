import {createSwitchNavigator,createStackNavigator,createDrawerNavigator} from 'react-navigation'

import Login from './../screens/login'
import Dashboard from './../screens/dashboard'
import AuthLoadingScreen from './../screens/authLoadingScreen'


const Auth = createStackNavigator({
    login:{screen:Login}
});


const App = createDrawerNavigator({
    Dashboard:Dashboard
})


export default root = createSwitchNavigator({
    splash: AuthLoadingScreen,
    
    auth: Auth, 

    app: App


},{
    initialRouteName: 'splash',
  })