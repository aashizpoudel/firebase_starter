import React, { Component } from 'react';
import { View, StyleSheet, TouchableNativeFeedback,AsyncStorage } from 'react-native';
import {  Title as Headline, Surface, Text, TextInput, Button } from 'react-native-paper';

class Login extends Component {
    state = {
        host: "",
        username: "",
        password: ""
    };

    _signInAsync = async () => {
        await AsyncStorage.setItem('userToken', 'abc');
        this.props.navigation.navigate('app');
      };

    componentDidMount(){
      
    }

    render() {
        return <View style={styles.container}>
        
            <View style={{ padding: 20, paddingBottom: 5 }}>
                <Headline>Dummy Login Page</Headline>
                <View style={styles.form}>

                    <Surface style={styles.formGroup}>
                        <TextInput mode="outlined" label="username" value={this.state.username} onChangeText={(e) => { this.setState({ username: e }) }} />
                    </Surface>


                    <Surface style={styles.formGroup}>

                        <TextInput mode="outlined" label="password" secureTextEntry={true} value={this.state.password} onChangeText={(e) => { this.setState({ password: e }) }} textContentType="password" />

                    </Surface>

                    <TouchableNativeFeedback>
                        <Button onPress={()=>{this._signInAsync()}} raised style={{marginBottom:10}} mode="contained">Login</Button>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback>
                        <Button raised color="red" mode="contained">Sign up</Button>
                    </TouchableNativeFeedback>
                </View>
            </View>
            
        </View>

    }
}

const styles = StyleSheet.create({
   container:{
    flex:1,

    // backgroundColor:'red'
   },
    formGroup: {
        padding: 10,
        marginBottom: 15,

    },
    form: {
        paddingTop: 30,
        paddingBottom: 30
    },

})
export default Login;