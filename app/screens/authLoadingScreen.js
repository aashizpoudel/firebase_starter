import React,{Component} from 'react';

import {View,ActivityIndicator,Text,AsyncStorage} from 'react-native';


class AuthLoading extends Component{

    constructor(props) {
        super(props);
        this._bootstrapAsync();
      }
    
    _bootstrapAsync = async () => {
        const userToken = await AsyncStorage.getItem('userToken');
    
        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.
        this.props.navigation.navigate(userToken ? 'app' : 'auth');
      };

    render(){
        return <View style={{padding:40,flex:1,justifyContent:'center',alignContent:'center'}}>
            <View >
            <ActivityIndicator />
            <Text>...loading...</Text></View>
        </View>
    }
}

export default AuthLoading;